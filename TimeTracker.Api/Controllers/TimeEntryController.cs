﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using TimeTracker.Api.Models;
using TimeTracker.Api.Services;

namespace TimeTracker.Api.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class TimeEntryController : ControllerBase
	{

		private readonly IConfiguration _configuration;
		private readonly IService<TimeEntry> _timeService;

		public TimeEntryController(IConfiguration configuration, IService<TimeEntry> timeService)
		{
			_configuration = configuration;
			_timeService = timeService;
		}

		// GET api/timeEntry
		[HttpGet]
		public ActionResult<IEnumerable<TimeEntry>> Get()
		{
			return Ok(_timeService.Get().ToList());
		}

		// GET api/timeEntry/5
		[HttpGet("{id}")]
		public ActionResult<TimeEntry> Get(int id)
		{
			var result = _timeService.Get(id);
			if (result == null)
			{
				return Ok(new NotFoundResult());
			}
			return Ok(result);
		}

		// POST api/timeEntry
		[HttpPost]
		public ActionResult<TimeEntry> Post([FromBody] TimeEntry timeEntry)
		{
			return Ok(_timeService.Create(timeEntry));
		}

		// PUT api/timeEntry/5
		[HttpPut("{id}")]
		public ActionResult<TimeEntry> Put(int id, [FromBody] TimeEntry timeEntry)
		{
			timeEntry.Id = id;
			var result = _timeService.Update(timeEntry);
			if (result == null)
			{
				return Ok(new NotFoundResult());
			}

			return Ok(result);
		}

		// DELETE api/timeEntry/5
		[HttpDelete("{id}")]
		public ActionResult Delete(int id)
		{
			_timeService.Delete(id);
			return Ok();
		}
	}
}
