﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using TimeTracker.Api.Models;
using TimeTracker.Api.Services;

namespace TimeTracker.Api.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ClientsController : ControllerBase
	{
		private readonly IConfiguration _configuration;
		private readonly IService<Client> _clientSerivceService;

		public ClientsController(IConfiguration configuration, IService<Client> clientService)
		{
			_configuration = configuration;
			_clientSerivceService = clientService;
		}

		// GET api/clients
		[HttpGet]
		public ActionResult<IEnumerable<Client>> Get()
		{
			return Ok(_clientSerivceService.Get().ToList());
		}

		// GET api/clients/5
		[HttpGet("{id}")]
		public ActionResult<Client> Get(int id)
		{
			var client = _clientSerivceService.Get(id);
			if (client == null) 
			{
				return Ok(new NotFoundResult());
			}

			return Ok(client);

		}

		// POST api/clients
		[HttpPost]
		public ActionResult<Client> Post([FromBody] Client client)
		{
			return Ok(_clientSerivceService.Create(client));
		}

		// PUT api/clients/5
		[HttpPut("{id}")]
		public ActionResult<Client> Put(int id, [FromBody] Client client)
		{
			client.Id = id;
			var updatedClient = _clientSerivceService.Update(client);
			if (updatedClient == null)
			{
				return Ok(new NotFoundResult());
			}

			return Ok(updatedClient);
		}

		// DELETE api/clients/5
		[HttpDelete("{id}")]
		public ActionResult Delete(int id)
		{
			_clientSerivceService.Delete(id);
			return Ok();
		}
	}
}
