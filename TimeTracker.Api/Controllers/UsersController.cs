﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using TimeTracker.Api.Models;
using TimeTracker.Api.Services;

namespace TimeTracker.Api.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class UsersController : ControllerBase
	{
		private readonly IConfiguration _configuration;
		private readonly IService<User> _userService;

		public UsersController(IConfiguration configuration, IService<User> userService)
		{
			_configuration = configuration;
			_userService = userService;
		}

		// GET api/User
		[HttpGet]
		public ActionResult<IEnumerable<User>> Get()
		{
			return Ok(_userService.Get().ToList());
		}

		// GET api/User/5
		[HttpGet("{id}")]
		public ActionResult<User> Get(int id)
		{
			return Ok(_userService.Get(id));
		}

		// POST api/User
		[HttpPost]
		public ActionResult<User> Post([FromBody] User user)
		{
			return Ok(_userService.Create(user));
		}

		// PUT api/User/5
		[HttpPut("{id}")]
		public ActionResult<User> Put(int id, [FromBody] User user)
		{
			user.Id = id;
			return Ok(_userService.Update(user));
		}

		// DELETE api/User/5
		[HttpDelete("{id}")]
		public ActionResult Delete(int id)
		{
			_userService.Delete(id);
			return Ok();
		}
	}
}

