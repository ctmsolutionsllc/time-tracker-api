﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace TimeTracker.Api.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class MetaController : ControllerBase
	{
		private readonly IConfiguration _configuration;

		public MetaController(IConfiguration configuration)
		{
			_configuration = configuration;
		}

		[HttpGet]
		[Route("ping")]
		public ActionResult<string> Ping()
		{
			return "pong";
		}

		[HttpGet]
		[Route("version/app")]
		public ActionResult<string> GetAppVersion()
		{
			return FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).ProductVersion;
		}

		[HttpGet]
		[Route("version/db")]
		public ActionResult<string> GetDbVersion()
		{
			var dbVersion = String.Empty;
			using (var connection = new SqlConnection(_configuration.GetConnectionString("TimeTracker")))
			{
				connection.Open();

				using (var command = connection.CreateCommand())
				{
					command.CommandText = "SELECT * FROM [Meta] WHERE [Name] = 'Version'";
					command.CommandType = CommandType.Text;
					using (var reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							dbVersion = reader.GetString(1);
						}
					}
				}
			}

			return dbVersion;
		}
	}
}
