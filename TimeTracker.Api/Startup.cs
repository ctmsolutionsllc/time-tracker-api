﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TimeTracker.Api.Models;
using TimeTracker.Api.Repository;
using TimeTracker.Api.Services;

namespace TimeTracker.Api
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		private IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddControllers();

			services.AddTransient<IRepository<User>, UserRepository>();
			services.AddTransient<IRepository<Client>, ClientRepository>();
			services.AddTransient<IRepository<TimeEntry>, TimeEntriesRepository>();
			services.AddTransient<IService<User>, UsersService>();
			services.AddTransient<IService<TimeEntry>, TimeEntriesService>();
			services.AddTransient<IService<Client>, ClientsService>();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseHttpsRedirection();

			app.UseRouting();

			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}
