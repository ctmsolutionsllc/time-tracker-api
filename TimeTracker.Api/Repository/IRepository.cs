﻿using System.Collections.Generic;

namespace TimeTracker.Api.Repository
{
	public interface IRepository<TEntity>
	{
		IEnumerable<TEntity> Get();
		TEntity Get(int id);
		TEntity Create(TEntity entity);
		TEntity Update(TEntity entity);
		void Delete(int id);
	}
}
