﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using TimeTracker.Api.Models;

namespace TimeTracker.Api.Repository
{
	public class UserRepository : IRepository<User>
	{
		private readonly IConfiguration _configuration;

		public UserRepository(IConfiguration configuration)
		{
			_configuration = configuration;
		}

		public IEnumerable<User> Get()
		{
			var users = new List<User>();
			using (var conn = new SqlConnection(_configuration.GetConnectionString("TimeTracker")))
			{
				conn.Open();
				using (var cmd = new SqlCommand("select * from [user]", conn))
				{
					using (var rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							users.Add(new User
							{
								Id = rdr.GetInt32(0),
								UserName = rdr.GetString(1),
								FirstName = rdr.GetString(2),
								LastName = rdr.GetString(3),
								CreatedOnUtc = rdr.GetDateTime(4)
							});
						}
					}
				}
			}
			return users;
		}

		public User Get(int id)
		{
			using (var conn = new SqlConnection(_configuration.GetConnectionString("TimeTracker")))
			{
				conn.Open();
				using (var cmd = new SqlCommand("select * from [user] where id = @id", conn))
				{
					var param = new SqlParameter { ParameterName = "@id", Value = id };
					cmd.Parameters.Add(param);
					using (var rdr = cmd.ExecuteReader())
					{
						if (!rdr.Read())
						{
							return null;
						}
						return new User
						{
							Id = rdr.GetInt32(0),
							UserName = rdr.GetString(1),
							FirstName = rdr.GetString(2),
							LastName = rdr.GetString(3),
							CreatedOnUtc = rdr.GetDateTime(4)
						};
					}
				}
			}
		}

		public User Get(String name)
		{
			using (var conn = new SqlConnection(_configuration.GetConnectionString("TimeTracker")))
			{
				using (var cmd = new SqlCommand("select * from [user] where userName = @name"))
				{
					var param = new SqlParameter { ParameterName = "@name", Value = name };
					cmd.Parameters.Add(param);
					using (var rdr = cmd.ExecuteReader())
					{
						if (!rdr.Read())
						{
							return null;
						}

						return new User
						{
							Id = rdr.GetInt32(0),
							UserName = rdr.GetString(1),
							FirstName = rdr.GetString(2),
							LastName = rdr.GetString(3),
							CreatedOnUtc = rdr.GetDateTime(4)
						};
					}
				}
			}
		}

		public User Create(User user)
		{
			using (var conn = new SqlConnection(_configuration.GetConnectionString("TimeTracker")))
			{
				conn.Open();
				using (var cmd =
					new SqlCommand(
						"insert into [user] (UserName, FirstName, LastName) values (@UserName, @FirstName, @LastName)",
						conn))
				{
					var userName = new SqlParameter();
					var firstName = new SqlParameter();
					var lastName = new SqlParameter();
					userName.ParameterName = "@UserName";
					firstName.ParameterName = "@FirstName";
					lastName.ParameterName = "@LastName";
					userName.Value = user.UserName;
					firstName.Value = user.FirstName;
					lastName.Value = user.LastName;
					cmd.Parameters.Add(userName);
					cmd.Parameters.Add(firstName);
					cmd.Parameters.Add(lastName);
					cmd.ExecuteNonQuery();
				}
			}

			return user;
		}

		public User Update(User user)
		{
			using (var conn = new SqlConnection(_configuration.GetConnectionString("TimeTracker")))
			{
				conn.Open();
				using (var cmd =
					new SqlCommand(
						"update [user] set UserName = @userName, FirstName = @firstName, LastName = @lastName where id = @id",
						conn))
				{
					var userName = new SqlParameter();
					var firstName = new SqlParameter();
					var lastName = new SqlParameter();
					var newId = new SqlParameter();
					userName.ParameterName = "@userName";
					userName.Value = user.UserName;
					firstName.ParameterName = "@firstName";
					firstName.Value = user.FirstName;
					lastName.ParameterName = "@lastName";
					lastName.Value = user.LastName;
					newId.ParameterName = "@id";
					newId.Value = user.Id;
					cmd.Parameters.Add(userName);
					cmd.Parameters.Add(firstName);
					cmd.Parameters.Add(lastName);
					cmd.Parameters.Add(newId);
					cmd.ExecuteNonQuery();
				}
			}

			return user;
		}

		public void Delete(int id)
		{
			using (var conn = new SqlConnection(_configuration.GetConnectionString("TimeTracker")))
			{
				conn.Open();
				using (var cmd = new SqlCommand("delete from [user] where id = @id", conn))
				{
					var param = new SqlParameter();
					param.ParameterName = "@id";
					param.Value = id;
					cmd.Parameters.Add(param);
					cmd.ExecuteNonQuery();
				}
			}
		}
	}
}
