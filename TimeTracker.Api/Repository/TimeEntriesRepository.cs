﻿using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data.SqlClient;
using TimeTracker.Api.Models;

namespace TimeTracker.Api.Repository
{
	public class TimeEntriesRepository : IRepository<TimeEntry>
	{

		private readonly IConfiguration _configuration;

		public TimeEntriesRepository(IConfiguration configuration)
		{
			_configuration = configuration;
		}

		public IEnumerable<TimeEntry> Get()
		{
			var entries = new List<TimeEntry>();
			using (var conn = new SqlConnection(_configuration.GetConnectionString("TimeTracker")))
			{
				conn.Open();
				using (var cmd = new SqlCommand("select * from TimeEntry", conn))
				{
					using (var rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							entries.Add(new TimeEntry
							{
								Id = rdr.GetInt32(0),
								UserId = rdr.GetInt32(1),
								ClientId = rdr.GetInt32(2),
								DateUtc = rdr.GetDateTime(3),
								Hours = rdr.GetDecimal(4),
								CreatedOnUtc = rdr.GetDateTime(5)
							});
						}
					}
				}
			}

			return entries;
		}

		public TimeEntry Get(int id)
		{
			using (var conn = new SqlConnection(_configuration.GetConnectionString("TimeTracker")))
			{
				conn.Open();
				using (var cmd = new SqlCommand("select * from TimeEntry where id = @id", conn))
				{
					var param = new SqlParameter { ParameterName = "@id", Value = id };
					cmd.Parameters.Add(param);
					using (var rdr = cmd.ExecuteReader())
					{
						if (!rdr.Read())
						{
							return null;
						}

						return new TimeEntry
						{
							Id = rdr.GetInt32(0),
							UserId = rdr.GetInt32(1),
							ClientId = rdr.GetInt32(2),
							DateUtc = rdr.GetDateTime(3),
							Hours = rdr.GetDecimal(4),
							CreatedOnUtc = rdr.GetDateTime(5)
						};
					}
				}
			}
		}

		public TimeEntry Create(TimeEntry timeEntry)
		{
			using (var conn = new SqlConnection(_configuration.GetConnectionString("TimeTracker")))
			{
				conn.Open();
				using (var cmd =
					new SqlCommand(
						"insert into TimeEntry (UserId, ClientId, DateUtc, Hours) values (@UserId, @ClientId, @Date, @Hours)",
						conn))
				{
					var userId = new SqlParameter();
					var clientId = new SqlParameter();
					var dateUtc = new SqlParameter();
					var hours = new SqlParameter();
					userId.ParameterName = "@UserId";
					clientId.ParameterName = "@ClientId";
					dateUtc.ParameterName = "@Date";
					hours.ParameterName = "@Hours";
					userId.Value = timeEntry.UserId;
					clientId.Value = timeEntry.ClientId;
					dateUtc.Value = timeEntry.DateUtc;
					hours.Value = timeEntry.Hours;
					cmd.Parameters.Add(userId);
					cmd.Parameters.Add(clientId);
					cmd.Parameters.Add(dateUtc);
					cmd.Parameters.Add(hours);
					cmd.ExecuteNonQuery();
				}
			}

			return timeEntry;
		}

		public TimeEntry Update(TimeEntry timeEntry)
		{
			using (var conn = new SqlConnection(_configuration.GetConnectionString("TimeTracker")))
			{
				conn.Open();
				using (var cmd = new SqlCommand("update TimeEntry set Hours = @Hours where id = @id", conn))
				{
					var hours = new SqlParameter();
					var newId = new SqlParameter();
					hours.ParameterName = "@Hours";
					hours.Value = timeEntry.Hours;
					newId.ParameterName = "@id";
					newId.Value = timeEntry.Id;
					cmd.Parameters.Add(hours);
					cmd.Parameters.Add(newId);
					return cmd.ExecuteNonQuery() == 0
						? null
						: timeEntry;
				}
			}
		}

		public void Delete(int id)
		{
			using (var conn = new SqlConnection(_configuration.GetConnectionString("TimeTracker")))
			{
				conn.Open();
				using (var cmd = new SqlCommand("delete from TimeEntry where id = @id", conn))
				{
					var param = new SqlParameter { ParameterName = "@id", Value = id };
					cmd.Parameters.Add(param);
					cmd.ExecuteNonQuery();
				}

			}
		}
	}
}
