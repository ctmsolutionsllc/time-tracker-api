﻿using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data.SqlClient;
using TimeTracker.Api.Models;

namespace TimeTracker.Api.Repository
{
	public class ClientRepository : IRepository<Client>
	{

		private readonly IConfiguration _configuration;

		public ClientRepository(IConfiguration configuration)
		{
			_configuration = configuration;
		}

		public IEnumerable<Client> Get()
		{
			var clients = new List<Client>();
			using (var conn = new SqlConnection(_configuration.GetConnectionString("TimeTracker")))
			{
				conn.Open();
				using (var cmd = new SqlCommand("select * from client", conn))
				{
					using (var rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							clients.Add(new Client()
							{
								Id = rdr.GetInt32(0),
								Name = rdr.GetString(1),
								CreatedOnUtc = rdr.GetDateTime(2)
							});
						}
					}
				}
			}

			return clients;
		}

		public Client Get(int id)
		{
			using (var conn = new SqlConnection(_configuration.GetConnectionString("TimeTracker")))
			{
				conn.Open();
				using (var cmd = new SqlCommand("select * from client where id = @id", conn))
				{
					var param = new SqlParameter { ParameterName = "@id", Value = id };
					cmd.Parameters.Add(param);
					using (var rdr = cmd.ExecuteReader())
					{
						if (!rdr.Read())
						{
							return null;
						}
						return new Client
						{
							Id = rdr.GetInt32(0),
							Name = rdr.GetString(1),
							CreatedOnUtc = rdr.GetDateTime(2)
						};
					}
				}
			}
		}

		public Client Create(Client client)
		{
			using (var conn = new SqlConnection(_configuration.GetConnectionString("TimeTracker")))
			{
				conn.Open();
				using (var cmd = new SqlCommand("insert into client (Name) values (@Name)", conn))
				{
					var name = new SqlParameter { ParameterName = "@Name", Value = client.Name };
					cmd.Parameters.Add(name);
					cmd.ExecuteNonQuery();
				}
			}
			return client;
		}

		public Client Update(Client client)
		{
			using (var conn = new SqlConnection(_configuration.GetConnectionString("TimeTracker")))
			{
				conn.Open();
				using (var cmd = new SqlCommand("update client set Name = @Name where id = @id", conn))
				{
					var name = new SqlParameter();
					var idParam = new SqlParameter();
					name.ParameterName = "@Name";
					name.Value = client.Name;
					idParam.ParameterName = "@id";
					idParam.Value = client.Id;
					cmd.Parameters.Add(name);
					cmd.Parameters.Add(idParam);
					return cmd.ExecuteNonQuery() == 0
						? null
						: client;
				}
			}
		}

		public void Delete(int id)
		{
			using (var conn = new SqlConnection(_configuration.GetConnectionString("TimeTracker")))
			{
				conn.Open();
				using (var cmd = new SqlCommand("delete from client where id = @id", conn))
				{
					var param = new SqlParameter { ParameterName = "@id", Value = id };
					cmd.Parameters.Add(param);
					cmd.ExecuteNonQuery();
				}
			}
		}
	}
}
