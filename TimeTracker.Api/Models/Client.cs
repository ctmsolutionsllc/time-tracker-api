﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace TimeTracker.Api.Models
{
	[ExcludeFromCodeCoverage]
	public class Client
	{
		public int Id { get; set; }
		[Required]
		public string Name { get; set; }
		public DateTime CreatedOnUtc { get; set; }
	}
}
