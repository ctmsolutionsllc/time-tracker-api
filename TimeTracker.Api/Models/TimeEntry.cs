﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace TimeTracker.Api.Models
{
	[ExcludeFromCodeCoverage]
	public class TimeEntry
	{
		public int Id { get; set; }
		public int UserId { get; set; }
		public int ClientId { get; set; }
		public DateTime DateUtc { get; set; }
		public decimal Hours { get; set; }
		public DateTime CreatedOnUtc { get; set; }
	}
}
