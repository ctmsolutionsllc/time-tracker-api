﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace TimeTracker.Api.Models
{
	[ExcludeFromCodeCoverage]
	public class User
	{
		public int Id { get; set; }
		[Required]
		public string UserName { get; set; }
		[Required]
		public string FirstName { get; set; }
		[Required]
		public string LastName { get; set; }
		public DateTime CreatedOnUtc { get; set; }
	}
}
