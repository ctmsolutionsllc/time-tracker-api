﻿using System;
using System.Collections.Generic;
using TimeTracker.Api.Models;
using TimeTracker.Api.Repository;

namespace TimeTracker.Api.Services
{
	public class UsersService : IService<User>
	{
		private readonly IRepository<User> _userRepository;

		public UsersService(IRepository<User> userRepository)
		{
			_userRepository = userRepository;
		}

		public IEnumerable<User> Get()
		{
			var result = _userRepository.Get();
			if (result == null)
			{
				throw new Exception("No users exist.");
			}
			return result;
		}
		
		public User Get(int id)
		{
			if (_userRepository.Get(id) == null)
			{
				throw new Exception("User does not exist.");
			}
			return _userRepository.Get(id);
		}
		
		public User Create(User user)
		{
			/*if (user.UserName != null && user.UserName.Equals(_userRepository.Get(user.UserName)))
			{
				throw new Exception("User already exists.");
			}*/
			return _userRepository.Create(user);
		}
		
		public User Update(User user)
		{
			if (_userRepository.Get(user.Id) == null)
			{
				throw new Exception("User does not exist.");
			}
			return _userRepository.Update(user);
		}

		public void Delete(int id)
		{
			if (_userRepository.Get(id) == null)
			{
				throw new Exception("User does not exist.");
			}
			_userRepository.Delete(id);
		}
	}
}
