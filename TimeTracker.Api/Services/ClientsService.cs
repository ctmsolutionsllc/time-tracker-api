﻿using System;
using System.Collections.Generic;
using TimeTracker.Api.Models;
using TimeTracker.Api.Repository;

namespace TimeTracker.Api.Services
{
	public class ClientsService : IService<Client>
	{
		private readonly IRepository<Client> _clientRepository;

		public ClientsService(IRepository<Client> clientRepository)
		{
			_clientRepository = clientRepository;
		}

		public IEnumerable<Client> Get()
		{
			var client = _clientRepository.Get();
			if (client == null)
			{
				throw new Exception("No clients exist.");
			}
			return client;
		}

		public Client Get(int id) {
			var client = _clientRepository.Get(id);
			if (client == null)
			{
				throw new Exception("Client does not exist.");
			}
			return client;
		} 

		public Client Create(Client client)
		{

			return _clientRepository.Create(client);
		}

		public Client Update(Client client)
		{
			if (_clientRepository.Get(client.Id) == null)
			{
				throw new Exception("Client does not exist.");
			}
			return _clientRepository.Update(client);
		}
		
		public void Delete(int id)
		{
			if (_clientRepository.Get(id) == null)
			{
				throw new Exception("Client does not exist.");
			}
			_clientRepository.Delete(id);
		}
	}
}
