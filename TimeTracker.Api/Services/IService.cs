﻿using System.Collections.Generic;

namespace TimeTracker.Api.Services
{
	public interface IService<TEntity>
	{
		IEnumerable<TEntity> Get();
		TEntity Get(int id);
		TEntity Create(TEntity entity);
		TEntity Update(TEntity entity);
		void Delete(int id);
	}
}
