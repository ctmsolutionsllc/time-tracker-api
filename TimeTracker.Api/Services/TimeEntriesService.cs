﻿using System;
using System.Collections.Generic;
using TimeTracker.Api.Models;
using TimeTracker.Api.Repository;

namespace TimeTracker.Api.Services
{
	public class TimeEntriesService : IService<TimeEntry>
	{
		private readonly IRepository<TimeEntry> _timeEntryRepository;

		public TimeEntriesService(IRepository<TimeEntry> timeEntryRepository)
		{
			_timeEntryRepository = timeEntryRepository;
		}

		public IEnumerable<TimeEntry> Get()
		{
			var result = _timeEntryRepository.Get();
			if (result == null)
			{
				throw new Exception("No time-entries exist.");
			}
			return result;
		}
		
		public TimeEntry Get(int id)
		{
			if (_timeEntryRepository.Get(id) == null)
			{
				throw new Exception("Time-entry does not exist.");
			}
			return _timeEntryRepository.Get(id);
		}
		
		public TimeEntry Create(TimeEntry timeEntry)
		{
			return _timeEntryRepository.Create(timeEntry);
		}
		
		public TimeEntry Update(TimeEntry timeEntry)
		{
			if (_timeEntryRepository.Get(timeEntry.Id) == null) 
			{
				throw new Exception("Time-entry does not exist.");
			}
			return _timeEntryRepository.Update(timeEntry);
		}
		
		public void Delete(int id)
		{
			if (_timeEntryRepository.Get(id) == null)
			{
				throw new Exception("Time-entry does not exist.");
			}
			_timeEntryRepository.Delete(id);
		}
	}
}
