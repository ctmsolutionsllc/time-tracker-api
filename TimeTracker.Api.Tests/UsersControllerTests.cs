﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using TimeTracker.Api.Controllers;
using TimeTracker.Api.Models;
using TimeTracker.Api.Services;

namespace TimeTracker.Api.Tests
{
	[TestClass]
	[ExcludeFromCodeCoverage]
	public class UsersControllerTests
	{
		private Mock<IConfiguration> _configurationMock;
		private Mock<IService<User>> _userServiceMock;
		private UsersController _target;

		#region Test Initialize and Cleanup

		[TestInitialize]
		public void TestInitialize()
		{
			_configurationMock = new Mock<IConfiguration>();
			_userServiceMock = new Mock<IService<User>>();
			_target = new UsersController(_configurationMock.Object, _userServiceMock.Object);
		}

		[TestCleanup]
		public void TestCleanup()
		{
			_configurationMock = null;
			_userServiceMock = null;
			_target = null;
		}

		#endregion Test Initialize and Cleanup

		#region Get (Many) Tests

		[TestMethod]
		public void Get_Many_Should_Get_From_Repository_As_Expected()
		{
			// Arrange

			// Act
			_target.Get();

			// Assert
			_userServiceMock.Verify(x => x.Get(), Times.Once);
		}

		[TestMethod]
		public void Get_Many_Should_Return_Expected_Results()
		{
			// Arrange
			var users = new List<User>
			{
				new User(),
				new User()
			};
			_userServiceMock.Setup(x => x.Get()).Returns(users);

			// Act
			var result = _target.Get();

			// Assert
			Assert.IsNotNull(result);
			Assert.IsInstanceOfType(result.Result, typeof(OkObjectResult));
			var okObjectResult = (OkObjectResult)result.Result;
			Assert.IsInstanceOfType(okObjectResult.Value, typeof(IEnumerable<User>));
			var resultUsers = (IEnumerable<User>) okObjectResult.Value;
			Assert.IsTrue(resultUsers.ToList().TrueForAll(x => users.Contains(x)));
		}

		#endregion Get (Many) Tests

		#region Get (One) Tests

		[TestMethod]
		public void Get_One_Should_Get_From_Repository_As_Expected()
		{
			// Arrange

			// Act
			_target.Get(1);

			// Assert
			_userServiceMock.Verify(x => x.Get(1), Times.Once);

		}

		[TestMethod]
		public void Get_One_Should_Get_Return_Expected_Result()
		{
			// Arrange
			var user = new User();
			_userServiceMock.Setup(x => x.Get(It.IsAny<int>())).Returns(user);


			// Act
			var result = _target.Get(1);

			// Assert
			Assert.IsNotNull(result);
			Assert.IsInstanceOfType(result.Result, typeof(OkObjectResult));
			var okObjectResult = (OkObjectResult)result.Result;
			Assert.IsInstanceOfType(okObjectResult.Value, typeof(User));
			var resultUser = (User)okObjectResult.Value;
			Assert.AreSame(user, resultUser);
		}

		#endregion Get (One) Tests

		#region Post Tests

		[TestMethod]
		public void Post_Should_Create_In_Repository_As_Expected()
		{
			// Arrange
			var user = new User();

			// Act
			_target.Post(user);

			// Assert
			_userServiceMock.Verify(x => x.Create(user), Times.Once);
		}

		[TestMethod]
		public void Post_Should_Return_Expected_Result()
		{
			// Arrange
			var user = new User();
			_userServiceMock.Setup(x => x.Create(It.IsAny<User>())).Returns(user);

			// Act
			var result = _target.Post(new User());

			// Assert
			Assert.IsNotNull(result);
			Assert.IsInstanceOfType(result.Result, typeof(OkObjectResult));
			var okObjectResult = (OkObjectResult)result.Result;
			Assert.IsInstanceOfType(okObjectResult.Value, typeof(User));
			var resultUser = (User)okObjectResult.Value;
			Assert.AreSame(user, resultUser);
		}

		#endregion Post Tests

		#region Put Tests

		[TestMethod]
		public void Put_Should_Update_In_Repository_As_Expected()
		{
			// Arrange
			var user = new User();

			// Act
			_target.Put(1, user);

			// Assert
			_userServiceMock.Verify(x => x.Update(It.Is<User>(y => ReferenceEquals(y, user) && user.Id == 1)), Times.Once);
		}

		[TestMethod]
		public void Put_Should_Return_Expected_Result()
		{
			// Arrange
			var user = new User();
			_userServiceMock.Setup(x => x.Update(It.IsAny<User>())).Returns(user);

			// Act
			var result = _target.Put(1, new User());

			// Assert
			Assert.IsNotNull(result);
			Assert.IsInstanceOfType(result.Result, typeof(OkObjectResult));
			var okObjectResult = (OkObjectResult)result.Result;
			Assert.IsInstanceOfType(okObjectResult.Value, typeof(User));
			var resultUser = (User)okObjectResult.Value;
			Assert.AreSame(user, resultUser);
		}

		#endregion Put Tests

		#region Delete Tests

		[TestMethod]
		public void Delete_Should_Delete_From_Repository_As_Expected()
		{
			// Arrange

			// Act
			_target.Delete(1);

			// Assert
			_userServiceMock.Verify(x => x.Delete(1), Times.Once);
		}

		[TestMethod]
		public void Delete_Should_Return_Expected_Result()
		{
			// Arrange

			// Act
			var result = _target.Delete(default(int));

			// Assert
			Assert.IsNotNull(result);
			Assert.IsInstanceOfType(result, typeof(OkResult));
		}

		#endregion Delete Tests
	}
}
