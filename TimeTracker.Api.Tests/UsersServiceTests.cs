﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using TimeTracker.Api.Models;
using TimeTracker.Api.Repository;
using TimeTracker.Api.Services;

namespace TimeTracker.Api.Tests
{
	[TestClass]
	[ExcludeFromCodeCoverage]
	public class UsersServiceTests
	{
		private Mock<IRepository<User>> _repositoryMock;
		private UsersService _target;

		#region Test Initialize and Cleanup

		[TestInitialize]
		public void TestInitialize()
		{
			_repositoryMock = new Mock<IRepository<User>>();
			_target = new UsersService(_repositoryMock.Object);
		}

		[TestCleanup]
		public void TestCleanup()
		{
			_repositoryMock = null;
			_target = null;
		}

		#endregion Test Initialize and Cleanup

		#region Get (Many) Tests

		[TestMethod]
		public void Get_Many_Should_Get_From_Repository_As_Expected()
		{
			var users = new List<User>();
			// Arrange
			_repositoryMock.Setup(x => x.Get()).Returns(users);
			// Act
			_target.Get();

			// Assert
			_repositoryMock.Verify(x => x.Get(), Times.Once);
		}

		[TestMethod]
		public void Get_Many_Should_Return_Expected_Results()
		{
			// Arrange
			var users = new List<User>();
			_repositoryMock.Setup(x => x.Get()).Returns(users);

			// Act 
			var results = _target.Get();

			// Assert
			Assert.AreSame(users, results);
		}

		[TestMethod]
		public void Get_Many_When_User_Not_Found_Should_Return_Expected_Results()
		{
			// Arrange
			_repositoryMock.Setup(x => x.Get()).Returns(null as IEnumerable<User>);

			// Act
			var result = Assert.ThrowsException<Exception>(() => _target.Get());

			// Assert
			Assert.AreEqual(result.Message, "No users exist.");
		}

		#endregion Get(Many) Tests

		#region Get(One) Tests

		[TestMethod]
		public void Get_One_Should_Get_From_Repository_As_Expected()
		{
			// Arrange
			var user = new User();
			user.Id = 1;
			_repositoryMock.Setup(x => x.Get(1)).Returns(user);

			// Act
			_target.Get(1);

			// Assert
			_repositoryMock.Verify(x => x.Get(1), Times.AtMost(2));
		}

		[TestMethod]
		public void Get_One_Should_Return_Expected_Results()
		{
			// Arrange
			var user = new User();
			user.Id = 1;
			_repositoryMock.Setup(x => x.Get(1)).Returns(user);

			// Act
			var results = _target.Get(1);

			// Assert
			Assert.AreSame(user, results);
		}

		[TestMethod]
		public void Get_One_When_User_Not_Found_Should_Return_Expected_Results()
		{
			// Arrange
			_repositoryMock.Setup(x => x.Get(1)).Returns(null as User);

			// Act
			var result = Assert.ThrowsException<Exception>(() => _target.Get(1));

			// Assert
			Assert.AreEqual(result.Message, "User does not exist.");
		}

		#endregion Get(One) Tests

		#region Create Tests

		[TestMethod]
		public void Create_Should_Create_In_Repository_As_Expected()
		{
			// Arrange
			var user = new User();
			user.Id = 1;

			// Act
			_target.Create(user);

			// Assert
			_repositoryMock.Verify(x => x.Create(user), Times.Once);
		}

		[TestMethod]
		public void Create_Should_Return_Expected_Results()
		{
			// Arrange
			var user = new User();
			user.Id = 1;
			_repositoryMock.Setup(x => x.Create(user)).Returns(user);

			// Act
			var results = _target.Create(user);

			// Assert
			Assert.AreSame(user, results);
		}

		#endregion Create Tests
		#region Update Tests

		[TestMethod]
		public void Update_Should_Update_In_Repository_As_Expected()
		{
			// Arrange
			var user = new User();
			user.Id = 1;
			_repositoryMock.Setup(mock => mock.Get(It.IsAny<int>())).Returns(new User());
			_repositoryMock.Setup(mock => mock.Update(It.IsAny<User>())).Returns(user);

			// Act
			var result = _target.Update(user);

			// Assert
			Assert.AreSame(user, result);
			_repositoryMock.Verify(mock => mock.Get(It.IsAny<int>()), Times.Once);
			_repositoryMock.Verify(mock => mock.Update(It.IsAny<User>()), Times.Once);
		}

		[TestMethod]
		public void Update_When_User_Not_Found_Should_Return_Expected_Results()
		{
			// Arrange
			var user = new User();
			user.Id = 1;
			_repositoryMock.Setup(x => x.Get(It.IsAny<int>())).Returns(null as User);

			// Act
			var result = Assert.ThrowsException<Exception>(() => _target.Update(user));

			// Assert
			Assert.AreEqual(result.Message, "User does not exist.");
		}

		#endregion Update Tests
		#region Delete Tests

		[TestMethod]
		public void Delete_Should_Delete_in_Repository_As_Expected()
		{
			// Arrange
			var user = new User();
			user.Id = 1;
			_repositoryMock.Setup(mock => mock.Get(It.IsAny<int>())).Returns(new User());
			_repositoryMock.Setup(mock => mock.Delete(It.IsAny<int>()));

			// Act
			_target.Delete(1);

			// Assert
			_repositoryMock.Verify(x => x.Delete(It.IsAny<int>()), Times.Once);
			_repositoryMock.Verify(x => x.Get(It.IsAny<int>()), Times.Once);
		}

		[TestMethod]
		public void Delete_When_TimeEntry_Not_Found_Should_Return_Expected_Results()
		{
			// Arrange
			_repositoryMock.Setup(x => x.Get(1)).Returns(null as User);

			// Act
			var result = Assert.ThrowsException<Exception>(() => _target.Delete(1));

			// Assert
			Assert.AreEqual(result.Message, "User does not exist.");
		}

		#endregion Delete Tests
	}
}
