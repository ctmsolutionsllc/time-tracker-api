﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using TimeTracker.Api.Models;
using TimeTracker.Api.Repository;
using TimeTracker.Api.Services;

namespace TimeTracker.Api.Tests
{
	[TestClass]
	[ExcludeFromCodeCoverage]
	public class ClientsServiceTests
	{
		private Mock<IRepository<Client>> _repositoryMock;
		private ClientsService _target;

		#region Test Initialize and Cleanup

		[TestInitialize]
		public void TestInitialize()
		{
			_repositoryMock = new Mock<IRepository<Client>>();
			_target = new ClientsService(_repositoryMock.Object);
		}

		[TestCleanup]
		public void TestCleanup()
		{
			_repositoryMock = null;
			_target = null;
		}

		#endregion Test Initialize and Cleanup

		#region Get (Many) Tests

		[TestMethod]
		public void Get_Many_Should_Get_From_Repository_As_Expected()
		{
			// Arrange
			// ReSharper disable once CollectionNeverUpdated.Local
			var clients = new List<Client>();
			_repositoryMock.Setup(x => x.Get()).Returns(clients);

			// Act
			_target.Get();

			// Assert
			_repositoryMock.Verify(x => x.Get(), Times.Once);
		}

		[TestMethod]
		public void Get_Many_Should_Return_Expected_Results()
		{
			// Arrange
			var clients = new List<Client>();
			_repositoryMock.Setup(x => x.Get()).Returns(clients);

			// Act 
			var results = _target.Get();

			// Assert
			Assert.AreSame(clients, results);
			_repositoryMock.Verify(x => x.Get(), Times.Once);
		}

		[TestMethod]
		public void Get_Many_When_Client_Not_Found_Should_Return_Expected_Results()
		{
			// Arrange
			_repositoryMock.Setup(x => x.Get()).Returns(null as IEnumerable<Client>);

			// Act
			var result = Assert.ThrowsException<Exception>(() => _target.Get());

			// Assert
			Assert.AreEqual(result.Message, "No clients exist.");
			_repositoryMock.Verify(x => x.Get(), Times.Once);
		}

		#endregion Get(Many) Tests

		#region Get(One) Tests

		[TestMethod]
		public void Get_One_Should_Get_From_Repository_As_Expected()
		{
			// Arrange
			const int clientId = 1;
			var client = new Client();
			_repositoryMock.Setup(mock => mock.Get(It.IsAny<int>())).Returns(client);

			// Act
			var result = _target.Get(clientId);

			// Assert
			Assert.AreSame(client, result);
			_repositoryMock.Verify(x => x.Get(clientId), Times.Once);
		}

		[TestMethod]
		public void Get_One_When_Client_Found_Should_Return_Expected_Results()
		{
			// Arrange
			var client = new Client();
			_repositoryMock.Setup(x => x.Get(It.IsAny<int>())).Returns(client);

			// Act
			var results = _target.Get(1);

			// Assert
			Assert.AreSame(client, results);
			_repositoryMock.Verify(x => x.Get(1), Times.Once);
		}

		[TestMethod]
		public void Get_One_When_Client_Not_Found_Should_Return_Expected_Results()
		{
			// Arrange
			_repositoryMock.Setup(x => x.Get(It.IsAny<int>())).Returns(null as Client);

			// Act
			var result = Assert.ThrowsException<Exception>(() => _target.Get(1));

			// Assert
			Assert.AreEqual(result.Message, "Client does not exist.");
			_repositoryMock.Verify(x => x.Get(1), Times.Once);
		}

		#endregion Get(One) Tests

		#region Create Tests

		[TestMethod]
		public void Create_Should_Create_In_Repository_As_Expected()
		{
			// Arrange
			var client = new Client();
			_repositoryMock.Setup(x => x.Create(client)).Returns(client);

			// Act
			var result = _target.Create(client);

			// Assert
			Assert.AreSame(result, client);
			_repositoryMock.Verify(x => x.Create(client), Times.Once);
		}

		[TestMethod]
		public void Create_Should_Return_Expected_Results()
		{
			// Arrange
			var client = new Client();
			_repositoryMock.Setup(x => x.Create(client)).Returns(client);

			// Act
			var results = _target.Create(client);

			// Assert
			Assert.AreSame(client, results);
			_repositoryMock.Verify(x => x.Create(client), Times.Once);
		}

		#endregion Create Tests

		#region Update Tests

		[TestMethod]
		public void Update_Should_Update_In_Repository_As_Expected()
		{
			// Arrange
			var client = new Client();
			client.Id = 1;
			client.Name = "Test";
			_repositoryMock.Setup(mock => mock.Get(It.IsAny<int>())).Returns(new Client());
			_repositoryMock.Setup(mock => mock.Update(It.IsAny<Client>())).Returns(client);

			// Act
			var result =_target.Update(client);

			// Assert
			Assert.AreSame(client, result);
			_repositoryMock.Verify(mock => mock.Get(client.Id), Times.Once);
			_repositoryMock.Verify(mock => mock.Update(client), Times.Once);
		}

		[TestMethod]
		public void Update_When_Client_Not_Found_Should_Return_Expected_Results()
		{
			// Arrange
			var client = new Client();
			_repositoryMock.Setup(x => x.Get(It.IsAny<int>())).Returns(null as Client);

			// Act
			var result = Assert.ThrowsException<Exception>(() => _target.Update(client));

			// Assert
			Assert.AreEqual(result.Message, "Client does not exist.");
			_repositoryMock.Verify(mock => mock.Get(client.Id), Times.Once);
		}

		#endregion Update Tests

		#region Delete Tests

		[TestMethod]
		public void Delete_Should_Delete_in_Repository_As_Expected()
		{
			// Arrange
			// ReSharper disable once UnusedVariable
			var client = new Client();
			_repositoryMock.Setup(mock => mock.Get(It.IsAny<int>())).Returns(new Client());
			_repositoryMock.Setup(mock => mock.Delete(It.IsAny<int>()));

			// Act
			_target.Delete(1);

			// Assert
			_repositoryMock.Verify(x => x.Delete(1), Times.Once);
			_repositoryMock.Verify(x => x.Get(1), Times.Once);
		}

		[TestMethod]
		public void Delete_When_Client_Not_Found_Should_Return_Expected_Results()
		{
			// Arrange
			_repositoryMock.Setup(x => x.Get(It.IsAny<int>())).Returns(null as Client);

			// Act
			var result = Assert.ThrowsException<Exception>(() => _target.Delete(1));

			// Assert
			Assert.AreEqual(result.Message, "Client does not exist.");
			_repositoryMock.Verify(x => x.Get(1), Times.Once);
		}

		#endregion Delete Tests
	}
}
