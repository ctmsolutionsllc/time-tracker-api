﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using TimeTracker.Api.Models;
using TimeTracker.Api.Repository;
using TimeTracker.Api.Services;

namespace TimeTracker.Api.Tests
{
	[TestClass]
	[ExcludeFromCodeCoverage]
	public class TimeEntriesServiceTests
	{
		private Mock<IRepository<TimeEntry>> _repositoryMock;
		private TimeEntriesService _target;

		#region Test Initialize and Cleanup

		[TestInitialize]
		public void TestInitialize()
		{
			_repositoryMock = new Mock<IRepository<TimeEntry>>();
			_target = new TimeEntriesService(_repositoryMock.Object);
		}

		[TestCleanup]
		public void TestCleanup()
		{
			_repositoryMock = null;
			_target = null;
		}

		#endregion Test Initialize and Cleanup

		#region Get (Many) Tests

		[TestMethod]
		public void Get_Many_Should_Get_From_Repository_As_Expected()
		{
			var timeEntries = new List<TimeEntry>();
			// Arrange
			_repositoryMock.Setup(x => x.Get()).Returns(timeEntries);

			// Act
			_target.Get();

			// Assert
			_repositoryMock.Verify(x => x.Get(), Times.Once);
		}

		[TestMethod]
		public void Get_Many_Should_Return_Expected_Results()
		{
			// Arrange
			var timeEntries = new List<TimeEntry>();
			_repositoryMock.Setup(x => x.Get()).Returns(timeEntries);

			// Act 
			var results = _target.Get();

			// Assert
			Assert.AreSame(timeEntries, results);
			_repositoryMock.Verify(x => x.Get(), Times.Once);
		}

		[TestMethod]
		public void Get_Many_When_Time_Entry_Not_Found_Should_Return_Expected_Results()
		{
			// Arrange
			_repositoryMock.Setup(x => x.Get()).Returns(null as IEnumerable<TimeEntry>);

			// Act
			var result = Assert.ThrowsException<Exception>(() => _target.Get());

			// Assert
			Assert.AreEqual(result.Message, "No time-entries exist.");
			_repositoryMock.Verify(x => x.Get(), Times.Once);
		}

		#endregion Get(Many) Tests

		#region Get(One) Tests

		[TestMethod]
		public void Get_One_Should_Get_From_Repository_As_Expected()
		{
			// Arrange
			var timeEntry = new TimeEntry();
			_repositoryMock.Setup(x => x.Get(It.IsAny<int>())).Returns(timeEntry);

			// Act
			_target.Get(1);

			// Assert
			_repositoryMock.Verify(x => x.Get(1), Times.Exactly(2));
		}

		[TestMethod]
		public void Get_One_Should_Return_Expected_Results()
		{
			// Arrange
			var timeEntry = new TimeEntry();
			_repositoryMock.Setup(x => x.Get(1)).Returns(timeEntry);

			// Act
			var results = _target.Get(1);

			// Assert
			Assert.AreSame(timeEntry, results);
			_repositoryMock.Verify(x => x.Get(1), Times.Exactly(2));
		}

		[TestMethod]
		public void Get_One_When_TimeEntry_Not_Found_Should_Return_Expected_Results()
		{
			// Arrange
			_repositoryMock.Setup(x => x.Get(It.IsAny<int>())).Returns(null as TimeEntry);

			// Act
			var result = Assert.ThrowsException<Exception>(() => _target.Get(1));

			// Assert
			Assert.AreEqual(result.Message, "Time-entry does not exist.");
			_repositoryMock.Verify(x => x.Get(1), Times.Exactly(1));
		}

		#endregion Get(One) Tests

		#region Create Tests

		[TestMethod]
		public void Create_Should_Create_In_Repository_As_Expected()
		{
			// Arrange
			var timeEntry = new TimeEntry();
			_repositoryMock.Setup(x => x.Create(timeEntry)).Returns(timeEntry);

			// Act
			var results = _target.Create(timeEntry);

			// Assert
			_repositoryMock.Verify(x => x.Create(timeEntry), Times.Once);
			Assert.AreSame(timeEntry, results);
		}

		[TestMethod]
		public void Create_Should_Return_Expected_Results()
		{
			// Arrange
			var timeEntry = new TimeEntry();
			_repositoryMock.Setup(x => x.Create(timeEntry)).Returns(timeEntry);

			// Act
			var results = _target.Create(timeEntry);

			// Assert
			_repositoryMock.Verify(x => x.Create(timeEntry), Times.Once);
			Assert.AreSame(timeEntry, results);
		}

		#endregion Create Tests

		#region Update Tests

		[TestMethod]
		public void Update_Should_Update_In_Repository_As_Expected()
		{
			// Arrange
			var timeEntry = new TimeEntry();
			timeEntry.Id = 1;
			_repositoryMock.Setup(mock => mock.Get(It.IsAny<int>())).Returns(new TimeEntry());
			_repositoryMock.Setup(mock => mock.Update(It.IsAny<TimeEntry>())).Returns(timeEntry);

			// Act
			var result = _target.Update(timeEntry);

			// Assert
			Assert.AreSame(timeEntry, result);
			_repositoryMock.Verify(mock => mock.Get(timeEntry.Id), Times.Once);
			_repositoryMock.Verify(mock => mock.Update(timeEntry), Times.Once);
		}

		[TestMethod]
		public void Update_When_TimeEntry_Not_Found_Should_Return_Expected_Results()
		{
			// Arrange
			var timeEntry = new TimeEntry();
			timeEntry.Id = 1;
			_repositoryMock.Setup(x => x.Get(It.IsAny<int>())).Returns(null as TimeEntry);

			// Act
			var result = Assert.ThrowsException<Exception>(() => _target.Update(timeEntry));

			// Assert
			Assert.AreEqual(result.Message, "Time-entry does not exist.");
			_repositoryMock.Verify(mock => mock.Get(timeEntry.Id), Times.Once);
		}

		#endregion Update Tests

		#region Delete Tests

		[TestMethod]
		public void Delete_Should_Delete_in_Repository_As_Expected()
		{
			// Arrange
			var timeEntry = new TimeEntry();
			timeEntry.Id = 1;
			_repositoryMock.Setup(mock => mock.Get(It.IsAny<int>())).Returns(new TimeEntry());
			_repositoryMock.Setup(mock => mock.Delete(It.IsAny<int>()));

			// Act
			_target.Delete(timeEntry.Id);

			// Assert
			_repositoryMock.Verify(x => x.Get(timeEntry.Id), Times.Once);
			_repositoryMock.Verify(x => x.Delete(timeEntry.Id), Times.Once);
		}

		[TestMethod]
		public void Delete_When_TimeEntry_Not_Found_Should_Return_Expected_Results()
		{
			// Arrange
			_repositoryMock.Setup(x => x.Get(1)).Returns(null as TimeEntry);

			// Act
			var result = Assert.ThrowsException<Exception>(() => _target.Delete(1));

			// Assert
			Assert.AreEqual(result.Message, "Time-entry does not exist.");
			_repositoryMock.Verify(x => x.Get(1), Times.Once);
		}

		#endregion Delete Tests
	}
}
