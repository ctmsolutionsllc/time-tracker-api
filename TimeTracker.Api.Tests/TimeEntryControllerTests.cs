﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using TimeTracker.Api.Controllers;
using TimeTracker.Api.Models;
using TimeTracker.Api.Services;

namespace TimeTracker.Api.Tests
{
	[TestClass]
	[ExcludeFromCodeCoverage]
	public class TimeEntryControllerTests
	{
		private Mock<IConfiguration> _configurationMock;
		private Mock<IService<TimeEntry>> _timeEntryRepositoryMock;
		private TimeEntryController _target;

		#region Test Initialize and Cleanup

		[TestInitialize]
		public void TestInitialize()
		{
			_configurationMock = new Mock<IConfiguration>();
			_timeEntryRepositoryMock = new Mock<IService<TimeEntry>>();
			_target = new TimeEntryController(_configurationMock.Object, _timeEntryRepositoryMock.Object);
		}

		[TestCleanup]
		public void TestCleanup()
		{
			_configurationMock = null;
			_timeEntryRepositoryMock = null;
			_target = null;
		}

		#endregion Test Initialize and Cleanup

		#region Get (Many) Tests

		[TestMethod]
		public void Get_Many_Should_Get_From_Repository_As_Expected()
		{
			// Arrange

			// Act
			_target.Get();

			// Assert
			_timeEntryRepositoryMock.Verify(x => x.Get(), Times.Once);
		}

		[TestMethod]
		public void Get_Many_Should_Return_Expected_Results()
		{

			// Arrange
			var timeEntries = new List<TimeEntry>
			{
				new TimeEntry(),
				new TimeEntry()
			};
			_timeEntryRepositoryMock.Setup(x => x.Get()).Returns(timeEntries);

			// Act
			var result = _target.Get();

			// Assert
			Assert.IsNotNull(result);
			Assert.IsInstanceOfType(result.Result, typeof(OkObjectResult));
			var okObjectResult = (OkObjectResult)result.Result;
			Assert.IsInstanceOfType(okObjectResult.Value, typeof(IEnumerable<TimeEntry>));
			var resultTimeEntries = (IEnumerable<TimeEntry>)okObjectResult.Value;
			Assert.IsTrue(resultTimeEntries.ToList().TrueForAll(x => timeEntries.Contains(x)));
		}

		#endregion Get (Many) Tests

		#region Get (One) Tests

		[TestMethod]
		public void Get_One_Should_Get_From_Repository_As_Expected()
		{
			// Arrange

			// Act
			_target.Get(1);

			// Assert
			_timeEntryRepositoryMock.Verify(x => x.Get(1), Times.Once);

		}

		[TestMethod]
		public void Get_One_Should_Get_Return_Expected_Result()
		{
			// Arrange
			var timeEntry = new TimeEntry();
			_timeEntryRepositoryMock.Setup(x => x.Get(It.IsAny<int>())).Returns(timeEntry);


			// Act
			var result = _target.Get(1);

			// Assert
			Assert.IsNotNull(result);
			Assert.IsInstanceOfType(result.Result, typeof(OkObjectResult));
			var okObjectResult = (OkObjectResult)result.Result;
			Assert.IsInstanceOfType(okObjectResult.Value, typeof(TimeEntry));
			var resultTimeEntry = (TimeEntry)okObjectResult.Value;
			Assert.AreSame(timeEntry, resultTimeEntry);
		}

		#endregion Get (One) Tests

		#region Post Tests

		[TestMethod]
		public void Post_Should_Create_In_Repository_As_Expected()
		{
			// Arrange
			var timeEntry = new TimeEntry();

			// Act
			_target.Post(timeEntry);

			// Assert
			_timeEntryRepositoryMock.Verify(x => x.Create(timeEntry), Times.Once);
		}

		[TestMethod]
		public void Post_Should_Return_Expected_Result()
		{
			// Arrange
			var timeEntry = new TimeEntry();
			_timeEntryRepositoryMock.Setup(x => x.Create(It.IsAny<TimeEntry>())).Returns(timeEntry);

			// Act
			var result = _target.Post(new TimeEntry());

			// Assert
			Assert.IsNotNull(result);
			Assert.IsInstanceOfType(result.Result, typeof(OkObjectResult));
			var okObjectResult = (OkObjectResult)result.Result;
			Assert.IsInstanceOfType(okObjectResult.Value, typeof(TimeEntry));
			var resultUser = (TimeEntry)okObjectResult.Value;
			Assert.AreSame(timeEntry, resultUser);
		}

		#endregion Post Tests

		#region Put Tests

		[TestMethod]
		public void Put_Should_Update_In_Repository_As_Expected()
		{
			// Arrange
			var timeEntry = new TimeEntry();

			// Act
			_target.Put(1, timeEntry);

			// Assert
			_timeEntryRepositoryMock.Verify(x => x.Update(It.Is<TimeEntry>(y => ReferenceEquals(y, timeEntry) && timeEntry.Id == 1)), Times.Once);
		}

		[TestMethod]
		public void Put_Should_Return_Expected_Result()
		{
			// Arrange
			var timeEntry = new TimeEntry();
			_timeEntryRepositoryMock.Setup(x => x.Update(It.IsAny<TimeEntry>())).Returns(timeEntry);

			// Act
			var result = _target.Put(1, new TimeEntry());

			// Assert
			Assert.IsNotNull(result);
			Assert.IsInstanceOfType(result.Result, typeof(OkObjectResult));
			var okObjectResult = (OkObjectResult)result.Result;
			Assert.IsInstanceOfType(okObjectResult.Value, typeof(TimeEntry));
			var resultTimeEntry = (TimeEntry)okObjectResult.Value;
			Assert.AreSame(timeEntry, resultTimeEntry);
		}

		#endregion Put Tests

		#region Delete Tests

		[TestMethod]
		public void Delete_Should_Delete_From_Repository_As_Expected()
		{
			// Arrange

			// Act
			_target.Delete(1);

			// Assert
			_timeEntryRepositoryMock.Verify(x => x.Delete(1), Times.Once);
		}

		[TestMethod]
		public void Delete_Should_Return_Expected_Result()
		{
			// Arrange

			// Act
			var result = _target.Delete(default(int));

			// Assert
			Assert.IsNotNull(result);
			Assert.IsInstanceOfType(result, typeof(OkResult));
		}

		#endregion Delete Tests
	}
}
