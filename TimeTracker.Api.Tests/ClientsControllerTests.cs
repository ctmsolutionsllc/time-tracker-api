﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using TimeTracker.Api.Controllers;
using TimeTracker.Api.Models;
using TimeTracker.Api.Services;

namespace TimeTracker.Api.Tests
{
	[TestClass]
	[ExcludeFromCodeCoverage]
	public class ClientsControllerTests
	{
		private Mock<IConfiguration> _configurationMock;
		private Mock<IService<Client>> _clientRepositoryMock;
		private ClientsController _target;

		#region Test Initialize and Cleanup

		[TestInitialize]
		public void TestInitialize()
		{
			_configurationMock = new Mock<IConfiguration>();
			_clientRepositoryMock = new Mock<IService<Client>>();
			_target = new ClientsController(_configurationMock.Object, _clientRepositoryMock.Object);
		}

		[TestCleanup]
		public void TestCleanup()
		{
			_configurationMock = null;
			_clientRepositoryMock = null;
			_target = null;
		}

		#endregion Test Initialize and Cleanup

		#region Get (Many) Tests

		[TestMethod]
		public void Get_Many_Should_Get_From_Repository_As_Expected()
		{
			// Arrange

			// Act
			_target.Get();

			// Assert
			_clientRepositoryMock.Verify(x => x.Get(), Times.Once);
		}

		[TestMethod]
		public void Get_Many_Should_Return_Expected_Results()
		{
			// Arrange
			var clients = new List<Client>
			{
				new Client(),
				new Client()
			};
			_clientRepositoryMock.Setup(x => x.Get()).Returns(clients);

			// Act
			var result = _target.Get();

			// Assert
			Assert.IsNotNull(result);
			Assert.IsInstanceOfType(result.Result, typeof(OkObjectResult));
			var okObjectResult = (OkObjectResult)result.Result;
			Assert.IsInstanceOfType(okObjectResult.Value, typeof(IEnumerable<Client>));
			var resultUsers = (IEnumerable<Client>)okObjectResult.Value;
			Assert.IsTrue(resultUsers.ToList().TrueForAll(x => clients.Contains(x)));
		}

		#endregion Get (Many) Tests

		#region Get (One) Tests

		[TestMethod]
		public void Get_One_Should_Get_From_Repository_As_Expected()
		{
			// Arrange

			// Act
			_target.Get(1);

			// Assert
			_clientRepositoryMock.Verify(x => x.Get(1), Times.Once);

		}

		[TestMethod]
		public void Get_One_Should_Get_Return_Expected_Result()
		{
			// Arrange
			var client = new Client();
			_clientRepositoryMock.Setup(x => x.Get(It.IsAny<int>())).Returns(client);


			// Act
			var result = _target.Get(1);

			// Assert
			Assert.IsNotNull(result);
			Assert.IsInstanceOfType(result.Result, typeof(OkObjectResult));
			var okObjectResult = (OkObjectResult)result.Result;
			Assert.IsInstanceOfType(okObjectResult.Value, typeof(Client));
			var resultClient = (Client)okObjectResult.Value;
			Assert.AreSame(client, resultClient);
		}


		#endregion Get (One) Tests

		#region Post Tests

		[TestMethod]
		public void Post_Should_Create_In_Repository_As_Expected()
		{
			// Arrange
			var client = new Client();

			// Act
			_target.Post(client);

			// Assert
			_clientRepositoryMock.Verify(x => x.Create(client), Times.Once);
		}

		[TestMethod]
		public void Post_Should_Return_Expected_Result()
		{
			// Arrange
			var client = new Client();
			_clientRepositoryMock.Setup(x => x.Create(It.IsAny<Client>())).Returns(client);

			// Act
			var result = _target.Post(new Client());

			// Assert
			Assert.IsNotNull(result);
			Assert.IsInstanceOfType(result.Result, typeof(OkObjectResult));
			var okObjectResult = (OkObjectResult)result.Result;
			Assert.IsInstanceOfType(okObjectResult.Value, typeof(Client));
			var resultClient = (Client)okObjectResult.Value;
			Assert.AreSame(client, resultClient);
		}

		#endregion Post Tests

		#region Put Tests

		[TestMethod]
		public void Put_Should_Update_In_Repository_As_Expected()
		{
			// Arrange
			var client = new Client();

			// Act
			_target.Put(1, client);

			// Assert
			_clientRepositoryMock.Verify(x => x.Update(It.Is<Client>(y => ReferenceEquals(y, client) && client.Id == 1)), Times.Once);
		}

		[TestMethod]
		public void Put_Should_Return_Expected_Result()
		{
			// Arrange
			var client = new Client();
			_clientRepositoryMock.Setup(x => x.Update(It.IsAny<Client>())).Returns(client);

			// Act
			var result = _target.Put(1, new Client());

			// Assert
			Assert.IsNotNull(result);
			Assert.IsInstanceOfType(result.Result, typeof(OkObjectResult));
			var okObjectResult = (OkObjectResult)result.Result;
			Assert.IsInstanceOfType(okObjectResult.Value, typeof(Client));
			var resultClient = (Client)okObjectResult.Value;
			Assert.AreSame(client, resultClient);
		}

		#endregion Put Tests

		#region Delete Tests

		[TestMethod]
		public void Delete_Should_Delete_From_Repository_As_Expected()
		{
			// Arrange

			// Act
			_target.Delete(1);

			// Assert
			_clientRepositoryMock.Verify(x => x.Delete(1), Times.Once);
		}

		[TestMethod]
		public void Delete_Should_Return_Expected_Result()
		{
			// Arrange

			// Act
			var result = _target.Delete(default(int));

			// Assert
			Assert.IsNotNull(result);
			Assert.IsInstanceOfType(result, typeof(OkResult));
		}

		#endregion Delete Tests
	}
}
