USE [Master]
GO

CREATE DATABASE [TimeTracker]
GO

USE [TimeTracker]
GO

CREATE TABLE [dbo].[Meta] (
  [Name] VARCHAR(20) NOT NULL,
  CONSTRAINT UC_Name UNIQUE([Name]),
  [Value] VARCHAR(20) NOT NULL
)
GO

CREATE TABLE [dbo].[User] (
  [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
  [UserName] VARCHAR(20) NOT NULL,
  CONSTRAINT UC_UserName UNIQUE([UserName]),
  [FirstName] VARCHAR(20) NOT NULL,
  [LastName] VARCHAR(20) NOT NULL,
  [CreatedOnUtc] DATETIME DEFAULT GETUTCDATE()
)
GO

CREATE TABLE [dbo].[Client] (
  [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
  [Name] VARCHAR(30) NOT NULL,
  [CreatedOnUtc] DATETIME DEFAULT GETUTCDATE()
)
GO

CREATE TABLE [dbo].[TimeEntry] (
  [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
  [UserId] INT FOREIGN KEY REFERENCES [User]([Id])
  ON UPDATE CASCADE,
  [ClientId] INT FOREIGN KEY REFERENCES [Client]([Id])
  ON UPDATE CASCADE,
  [Date] DATE NOT NULL,
  [Hours] DECIMAL(4,2) NOT NULL,
  [CreatedOnUtc] DATETIME DEFAULT GETUTCDATE()
)
GO


INSERT INTO [dbo].[User] ([UserName], [FirstName], [LastName]) VALUES ('UserName1', 'FirstName1', 'LastName1')
INSERT INTO [dbo].[User] ([UserName], [FirstName], [LastName]) VALUES ('UserName2', 'FirstName2', 'LastName2')

INSERT INTO [dbo].[Client] ([Name]) VALUES ('Name1')
INSERT INTO [dbo].[Client] ([Name]) VALUES ('Name2')

INSERT INTO [dbo].[TimeEntry] ([UserId], [ClientId], [Date], [Hours]) VALUES (1, 1, '2019-05-13', 1.5)
INSERT INTO [dbo].[TimeEntry] ([UserId], [ClientId], [Date], [Hours]) VALUES (2, 2, '2019-05-13', 2.0)
GO
